package generadornumeros;

/**
 *
 * @author luismiguel
 */
import java.awt.Color;
import java.util.Random;
import javax.swing.*;

public class GeneradorNumeros extends Thread {

    double a, c, m, x1, xn;
    int op,iteraciones;
    boolean semilla;
    static String ri;
    boolean cs = true;
    public JTextArea pantalla;
    public JPanel panel;
    private final Random random= new Random();

    public static String aleatorio(int xn1, int m) {
        String r = xn1+"/"+m;
        if(xn1==0){
        r="0";
        }
        return (r);
    }

    public static double formula(double a, double c, double m, double xn) {
        double xn1 = (a * xn + c) % m;
        return (xn1);

    }

    public GeneradorNumeros(boolean semillac, JTextField jm,
            JTextField jb, JTextField jxn,JTextField iter, JTextField jsemilla,
            JTextArea pantalla,JPanel panel) {

        super("GeneradorNumeros");
        this.pantalla=pantalla;
        this.panel=panel;

        double semilla = Double.parseDouble(jsemilla.getText());
        m = Double.parseDouble(jm.getText());
        c = Double.parseDouble(jb.getText());
        xn = Double.parseDouble(jxn.getText());
        iteraciones = Integer.parseInt(iter.getText());
       
        if (semillac) {
            a = semilla;
        } else {
            a = semilla;
        }
    }
    
    private Color agregar(){
        int r = random.nextInt(255);
        int g = random.nextInt(255);
        int b = random.nextInt(255);

        return new Color(r,g,b);
}

    public void run() {
        if (a == 0 || xn == 0 || c == 0) {
            JOptionPane.showMessageDialog(null, "Compruebe que \"a\",\" Xn\" o \"c\" sean mayores a 0",
                    "Error", JOptionPane.WARNING_MESSAGE);

        } else {
           // if (m > a && m > c && m > xn) {
                while (cs) {

                    x1 = formula(a, c, m, xn);
                    ri = aleatorio((int)x1,(int) m);
                    
            //System.out.println(ri);

                    String guion="-";
                    try {
                        pantalla.setForeground(agregar());
                        
                        panel.setBackground(agregar());
                        for(int i = 0;i<=130;i++){
                        System.out.print("-");}
                        double aleatorio=x1/m;
                        int multiplicacion=(int)a*(int)xn+(int)c;
                        String form= multiplicacion+"/"+(int)m;
                        //System.out.println("\nIteracion\n"+op+"\t a:\n\t" + a + " " + 
                          //      "Xno:" + xn + " " + "Xn1:" + x1);
                        System.out.println("\nIteracion\ta:\tXn0\tXn1\tAleatorio\tXn+1=(a*Xn+c)mod m");
                        System.out.println(op+"\t"+a+"\t"+xn+"\t"+x1+"\t"+ri+
                                "\t           "+form);
                        
                        xn = x1;
                        
                        op++;
                //System.out.println("FFFFF"+op+"sdddddd"+iteraciones);
                        if(op==iteraciones){
                        cs=false;}
                        if(iteraciones>=60){Thread.sleep(100);}
                        if(iteraciones<=60){Thread.sleep(300);}
                        if(iteraciones>=300){Thread.sleep(5);}
                        
                        

                //if(x1==xn){
                        //cs=false;}
                    } catch (Exception ex) {
                    }

                }
           // } else {
              //  JOptionPane.showMessageDialog(null, "Compruebe que \"a\",\" Xn\" o \"c\" sean menores a m",
                //        "Error", JOptionPane.WARNING_MESSAGE);
            //}
        }

    }

}
